﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Kungly.KinectVideoRecorder;
using VideoTools;
using Windows.ApplicationModel;
using System.Threading;
using WindowsPreview.Kinect;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;

namespace UnitTestLibrary
{
    [TestClass]
    public class KinectRecorderTest
    {

        [TestMethod]
        public async Task ReadJPGWriteMP4()
        {
            var path = await Package.Current.InstalledLocation.GetFolderAsync("Images");
            var filePath = "1080p.jpg";
            // Read file
            var file = await path.GetFileAsync(filePath);
            byte[] bytes;
            using (IRandomAccessStream stream = await file.OpenReadAsync())
            {
                var decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.JpegDecoderId, stream);
                var data = await decoder.GetPixelDataAsync();
                bytes = data.DetachPixelData();
            }

            StorageFile outputFile = await KnownFolders.VideosLibrary.CreateFileAsync(string.Format(
                    "{0}_{1}.mp4",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss_fff"),
                    Guid.NewGuid()), CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream outStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                    CoreDispatcherPriority.Normal, () =>
                    {
                        using (var video = new VideoWriter(1920, 1080, outStream, 100))
                        {
                            for (int i = 0; i < 50; i++)
                            {
                                video.AppendNewFrame(bytes);
                            }
                            video.Finalize();
                        }
                    });
            }

        }
        [TestMethod]
        public async Task ReadJPGWriteWMV()
        {
            var path = await Package.Current.InstalledLocation.GetFolderAsync("Images");
            var filePath = "1080p.jpg";
            // Read file
            var file = await path.GetFileAsync(filePath);
            byte[] bytes;
            using (IRandomAccessStream stream = await file.OpenReadAsync())
            {
                var decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.JpegDecoderId, stream);
                var data = await decoder.GetPixelDataAsync();
                bytes = data.DetachPixelData();
            }

            StorageFile outputFile = await KnownFolders.VideosLibrary.CreateFileAsync(string.Format(
                    "{0}_{1}.wmv",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss_fff"),
                    Guid.NewGuid()), CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream outStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                    CoreDispatcherPriority.Normal, () =>
                    {
                        using (var video = new VideoGenerator(1920, 1080, outStream, 16))
                        {
                            for (int i = 0; i < 200; i++)
                            {
                                video.AppendNewFrame(bytes);
                            }
                            video.Finalize();
                        }
                    });
            }

        }

        [TestMethod]
        public async Task ReadJPGWriteJPG()
        {
            var path = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Images");
            var filePath = "1080p.jpg";
            // Read file


            var file = await path.GetFileAsync(filePath);
            byte[] bytes;
            using (IRandomAccessStream stream = await file.OpenReadAsync())
            {
                var decoder = await BitmapDecoder.CreateAsync(BitmapDecoder.JpegDecoderId, stream);
                var data = await decoder.GetPixelDataAsync();
                bytes = data.DetachPixelData();
            }


            // Must create WriteableBitmap from Dispatcher thread
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal, async () =>
                {
                    var bitmap = new WriteableBitmap(1920, 1080);
                    using (var pixelStream = bitmap.PixelBuffer.AsStream())
                    {
                        await pixelStream.WriteAsync(bytes, 0, bytes.Length);
                    }

                    await bitmap.SaveToFile();
                });

        }

        private async Task<bool> FileExists(StorageFolder folder, string fileName)
        {
            var item = await ApplicationData.Current.LocalFolder.TryGetItemAsync(fileName);
            return item != null;
        }

        private static async Task EncodeWriteableBitmap(WriteableBitmap bmp, IRandomAccessStream writeStream, Guid encoderId)
        {
            // Copy buffer to pixels
            byte[] pixels;
            using (var stream = bmp.PixelBuffer.AsStream())
            {
                pixels = new byte[(uint)stream.Length];
                await stream.ReadAsync(pixels, 0, pixels.Length);
            }

            // Encode pixels into stream
            var encoder = await BitmapEncoder.CreateAsync(encoderId, writeStream);
            encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied,
               (uint)bmp.PixelWidth, (uint)bmp.PixelHeight,
               96, 96, pixels);
            await encoder.FlushAsync();

        }


    }
}
