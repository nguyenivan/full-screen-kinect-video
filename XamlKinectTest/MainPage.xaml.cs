﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using VideoTools;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WindowsPreview.Kinect;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace XamlKinectTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        KinectSensor _sensor;
        MultiSourceFrameReader _multiReader;
        System.Collections.Concurrent.ConcurrentQueue<byte[]> _rawQueue = new System.Collections.Concurrent.ConcurrentQueue<byte[]>();
        System.Collections.Concurrent.ConcurrentQueue<byte[]> _convertedQueue = new System.Collections.Concurrent.ConcurrentQueue<byte[]>();
        int _count = 0;
        static int QUEUE_SIZE = 50;

        public MainPage()
        {
            this.InitializeComponent();
            TextBoxUtilities.SetAlwaysScrollToEnd(LogTextBox, true);
            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                _sensor.Open();
                _multiReader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color);
                _multiReader.MultiSourceFrameArrived += multiReader_MultiSourceFrameArrived;

            }




        }

        public void Log(string message)
        {
            LogTextBox.Text = LogTextBox.Text + message + Environment.NewLine;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }

        private void multiReader_MultiSourceFrameArrived(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs args)
        {
            using (var frame = args.FrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    using (var cFrame = frame.ColorFrameReference.AcquireFrame())
                    {
                        if (cFrame != null)
                        {
                            if (_count < QUEUE_SIZE)
                            {
                                var cFrameFormat = cFrame.RawColorImageFormat;
                                var cFrameDesc = cFrame.CreateFrameDescription(cFrameFormat);
                                var cFrameSize = cFrameDesc.BytesPerPixel * cFrameDesc.Width * cFrameDesc.Height;
                                byte[] cFrameData = new byte[cFrameSize];
                                cFrame.CopyRawFrameDataToArray(cFrameData);
                                Log(string.Format("Queuing Kinect raw frame {0}", _count));
                                _rawQueue.Enqueue(cFrameData);
                                _count++;
                            }
                            else if (_count < QUEUE_SIZE * 2)
                            {
                                var cFrameFormat = ColorImageFormat.Rgba;
                                var cFrameDesc = cFrame.CreateFrameDescription(cFrameFormat);
                                var cFrameSize = cFrameDesc.BytesPerPixel * cFrameDesc.Width * cFrameDesc.Height;
                                byte[] cFrameData = new byte[cFrameSize];
                                cFrame.CopyConvertedFrameDataToArray(cFrameData, cFrameFormat);
                                Log(string.Format("Queuing Kinect converted frame {0}", _count));
                                _convertedQueue.Enqueue(cFrameData);
                                _count++;
                            }
                            else
                            {
                                _multiReader.IsPaused = true;
                                writeVideo();
                            }
                        }
                    }
                }
            }
        }

        private async void writeVideo()
        {
            StorageFile outputFile = await KnownFolders.VideosLibrary.CreateFileAsync(string.Format(
                    "{0}_raw.mp4",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss_fff")
                    ), CreationCollisionOption.ReplaceExisting);
            TimeSpan t1, t2;
            using (IRandomAccessStream outStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var start1 = DateTime.Now;
                using (var video = new VideoWriter(1920, 1080, outStream, 50))
                {
                    byte[] frameData = null;
                    int i = 0;
                    while (_rawQueue.TryDequeue(out frameData))
                    {
                        Log(string.Format("Writing raw video frame {0}", i++));
                        video.AppendYUY2Frame(frameData);
                    }
                    video.Finalize();
                }
                t1 = DateTime.Now - start1;
            }

            outputFile = await KnownFolders.VideosLibrary.CreateFileAsync(string.Format(
                    "{0}_converted.wmv",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss_fff")
                    ), CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream outStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var start2 = DateTime.Now;
                using (var video = new VideoGenerator(1920, 1080, outStream, 50))
                {
                    byte[] frameData = null;
                    int i = 0;
                    while (_convertedQueue.TryDequeue(out frameData))
                    {
                        Log(string.Format("Writing converted video frame {0}", i++));

                        video.AppendNewFrame(frameData);
                    }
                    video.Finalize();
                }
                t2 = DateTime.Now - start2;
            }

            Log(string.Format("Time for H.264 encoding: {0} seconds.", t1.TotalSeconds));
            Log(string.Format("Time for WMV encoding: {0} seconds." , t2.TotalSeconds));
        }


    }
}
