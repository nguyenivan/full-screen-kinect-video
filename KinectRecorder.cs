﻿using AForge.Video.FFMPEG;
using Microsoft.Kinect;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kungly.FullScreenKinectVideo
{
    public class KinectRecorder : IDisposable
    {
        private static readonly string FILE_EXTENSION = ".mp4";
        private KinectSensor _sensor;
        private SemaphoreSlim _semaphore = new SemaphoreSlim(1);
        private ConcurrentQueue<Bitmap> _recordQueue = new ConcurrentQueue<Bitmap>();
        private bool _isStarted = false;
        private bool _isStopped = false;
        private Task _processFramesTask = null;
        private CancellationTokenSource _processFramesCancellationTokenSource = new CancellationTokenSource();

        private DateTime _previousFlushTime;

        private bool _enableBodyRecorder;
        private bool _enableColorRecorder;
        private bool _enableDepthRecorder;
        private bool _enableInfraredRecorder;
        private string _path;
        private string _fileName;
        private MultiSourceFrameReader _multiReader;
        private FrameDescription _colorDescription;
        private uint _frameSize;
        private VideoFileWriter _writer;

        ////////////////////////////////////////////////////////////////////////////
        #region PROPERTIES
        ////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Indicates whether the recorder is currently started. Will be true
        /// any time between the calls to Start() and StopAsync().
        /// </summary>
        public bool IsStarted
        {
            get { return _isStarted; }
        }

        /// <summary>
        /// Determines whether the KinectRecorder will record Body frames. Applies only when the
        /// KinectRecorder is in "Automatic" mode. Cannot be changed after recording has started.
        /// </summary>
        public bool EnableBodyRecorder
        {
            get { return _enableBodyRecorder; }
            set
            {
                if (_isStarted)
                    throw new InvalidOperationException("Cannot modify recorder properties after recording has started");

                _enableBodyRecorder = value;
            }
        }

        /// <summary>
        /// Determines whether the KinectRecorder will record Color frames. Applies only when the
        /// KinectRecorder is in "Automatic" mode. Cannot be changed after recording has started.
        /// </summary>
        public bool EnableColorRecorder
        {
            get { return _enableColorRecorder; }
            set
            {
                if (_isStarted)
                    throw new InvalidOperationException("Cannot modify recorder properties after recording has started");

                _enableColorRecorder = value;
            }
        }

        /// <summary>
        /// Determines whether the KinectRecorder will record Depth frames. Applies only when the
        /// KinectRecorder is in "Automatic" mode. Cannot be changed after recording has started.
        /// </summary>
        public bool EnableDepthRecorder
        {
            get { return _enableDepthRecorder; }
            set
            {
                if (_isStarted)
                    throw new InvalidOperationException("Cannot modify recorder properties after recording has started");

                _enableDepthRecorder = value;
            }
        }

        /// <summary>
        /// Determines whether the KinectRecorder will record Infrared frames. Applies only when the
        /// KinectRecorder is in "Automatic" mode. Cannot be changed after recording has started.
        /// </summary>
        public bool EnableInfraredRecorder
        {
            get { return _enableInfraredRecorder; }
            set
            {
                if (_isStarted)
                    throw new InvalidOperationException("Cannot modify recorder properties after recording has started");

                _enableInfraredRecorder = value;
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////
        #region CONSTRUCTOR / DESTRUCTOR
        ////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// <para>
        ///     Creates a new instance of a <c>KinectRecorder</c> which can save frames to
        ///     the referenced stream.
        /// </para>
        /// <para>
        ///     The KinectRecorder can operate in two distinct modes. The "Automatic" mode
        ///     requires only that you pass a valid <c>KinectSensor</c> object to this
        ///     constructor. Recording of frames for each enabled frame type happens automatically
        ///     between the time Start() and StopAsync() are called.
        /// </para>
        /// <para>
        ///     In certain situations, the developer may wish to have more precise control
        ///     over when and how frames are recorded. If no <c>KinectSensor</c> is passed in
        ///     to this constructor, Start() and StopAsync() must still be called to begin and
        ///     end the recording session. However, the KinectRecorder will be in "Manual" mode,
        ///     and frames are recorded only when passed in to the RecordFrame() method.
        /// </para>
        /// </summary>
        /// <param name="stream">
        ///     The stream to which frames will be stored.
        /// </param>
        /// <param name="sensor">
        ///     If supplied, the <c>KinectSensor</c> from which frames will be automatically
        ///     retrieved for recording.
        /// </param>
        public KinectRecorder(string path = null, string fileName = null, KinectSensor sensor = null)
        {
            _path = this.GetDirectory(path);
            _fileName = this.GetFileName(fileName);
            _sensor = sensor;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="KinectRecorder"/> class.
        /// </summary>
        ~KinectRecorder()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    _semaphore.Wait();
                    if (_writer != null)
                    {
                        _writer.Dispose();
                        _writer = null;
                    }
                }
                catch (Exception ex)
                {
                    // TODO: Change to log the error
                    System.Diagnostics.Debug.WriteLine(ex);
                }
                finally
                {
                }

                if (_processFramesCancellationTokenSource != null)
                {
                    _processFramesCancellationTokenSource.Dispose();
                    _processFramesCancellationTokenSource = null;
                }
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////
        #region PUBLIC METHODS
        ////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Start the <c>KinectRecorder</c> session. This will write the file header and
        /// enable the recorder to begin processing frames.
        /// </summary>
        public void Start()
        {
            if (_isStarted)
                return;

            if (_isStopped)
                throw new InvalidOperationException("Cannot restart a recording after it has been stopped");

            if (_sensor != null)
            {
                FrameSourceTypes sourceType = FrameSourceTypes.None;

                if (EnableColorRecorder)
                {
                    sourceType = sourceType | FrameSourceTypes.Color;
                    _colorDescription = _sensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Rgba);
                    _frameSize = _colorDescription.BytesPerPixel * _colorDescription.LengthInPixels;

                }

                if (EnableBodyRecorder)
                {
                    sourceType = sourceType | FrameSourceTypes.Body;
                }

                if (EnableDepthRecorder)
                {
                    sourceType = sourceType | FrameSourceTypes.Depth;
                }

                if (EnableInfraredRecorder)
                {
                    sourceType = sourceType | FrameSourceTypes.Infrared;
                }

                if (sourceType == FrameSourceTypes.None)
                {
                    throw new InvalidOperationException("One of Kinect streams must be enabled.");
                }

                _multiReader = _sensor.OpenMultiSourceFrameReader(sourceType);
                _multiReader.MultiSourceFrameArrived += _multiReader_MultiSourceFrameArrived;

                _writer = new VideoFileWriter();

                _writer.Open(this.FullPath, _colorDescription.Width, _colorDescription.Height, 30, VideoCodec.MPEG4, 8000000);


                if (!_sensor.IsOpen)
                    _sensor.Open();

            }

            _isStarted = true;

            _processFramesTask = ProcessFramesAsync();
        }

        public Task RecordAsync(Bitmap frame)
        {
            //_isStarted = true;
            return Task.Run(() =>
            {
                try
                {
                    //string newName = string.Format("{0}{1}", ++_frameCount, _fileName);
                    //// Header
                    //using (Stream baseStream = new FileStream(Path.Combine(_path, newName), FileMode.Create))
                    //{
                    //    frame.Save(baseStream, ImageFormat.Jpeg);
                    //}

                    //using (Bitmap bmap = new Bitmap(colorFrameDescription.Width, colorFrameDescription.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb))
                    _writer.WriteVideoFrame(frame);
                }
                catch (Exception ex)
                {
                    // TODO: Change to log the error
                    System.Diagnostics.Debug.WriteLine(ex);
                }
            });
        }

        /// <summary>
        /// Stops the <c>KinectRecorder</c>, writes all frames remaining in the
        /// record queue, and closes the associated stream.
        /// </summary>
        public async Task StopAsync()
        {
            if (_isStopped)
                return;

            System.Diagnostics.Debug.WriteLine(">>> StopAsync (queue size {0})", _recordQueue.Count);

            _isStarted = false;
            _isStopped = true;

            if (_multiReader != null)
            {
                _multiReader.MultiSourceFrameArrived -= _multiReader_MultiSourceFrameArrived;
                _multiReader.Dispose();
                _multiReader = null;
            }

            try
            {
                await _processFramesTask;
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("!!! Process Canceled (in StopAsync)");
            }
            _processFramesTask = null;

            await CloseWriterAsync();

            System.Diagnostics.Debug.WriteLine("<<< StopAsync (DONE!)");
        }

        /// <summary>
        /// Stops the <c>KinectRecorder</c>, discards all remaining frames in the
        /// record queue, and closes the associated stream.
        /// </summary>
        public async void CancelAsync()
        {
            if (_processFramesTask == null)
                return;

            System.Diagnostics.Debug.WriteLine(">>> CancelAsync (queue size {0})", _recordQueue.Count);

            _isStarted = false;
            _isStopped = true;

            try
            {
                _processFramesCancellationTokenSource.Cancel();
                await _processFramesTask;
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("--- Cancel Acknowledged");
            }
            _processFramesTask = null;

            await CloseWriterAsync();

            System.Diagnostics.Debug.WriteLine("<<< CancelAsync (DONE!)");
        }

        private async Task CloseWriterAsync()
        {
            try
            {
                await _semaphore.WaitAsync();
                if (_writer != null)
                {
                    _writer.Dispose();
                    _writer = null;
                }
            }
            catch (Exception ex)
            {
                // TODO: Change to log the error
                System.Diagnostics.Debug.WriteLine(ex);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////
        #region PRIVATE METHODS
        ////////////////////////////////////////////////////////////////////////////

        private async Task ProcessFramesAsync()
        {
            _previousFlushTime = DateTime.Now;
            var cancellationToken = _processFramesCancellationTokenSource.Token;
            await Task.Run(async () =>
            {
                while (true)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    Bitmap frame;
                    if (_recordQueue.TryDequeue(out frame) && frame != null)
                    {
                        try
                        {
                            await _semaphore.WaitAsync();
                            await this.RecordAsync(frame);
                            System.Diagnostics.Debug.WriteLine("--- Processed  Frame ({0})", _recordQueue.Count);
                            Flush();

                        }
                        catch (Exception ex)
                        {
                            // TODO: Change to log the error
                            System.Diagnostics.Debug.WriteLine(ex);
                        }
                        finally
                        {
                            _semaphore.Release();
                            frame.Dispose();
                        }
                    }
                    else
                    {
                        await Task.Delay(100);
                        System.Diagnostics.Debug.WriteLine(_recordQueue.IsEmpty);
                        if (_recordQueue.IsEmpty && _isStarted == false)
                        {
                            break;
                        }
                    }
                }
            }, cancellationToken).ConfigureAwait(false);
        }

        private void Flush()
        {
            var now = DateTime.Now;

            if (now.Subtract(_previousFlushTime).TotalSeconds > 10)
            {
                _previousFlushTime = now;
                ///TODO Flush to video
            }
        }


        void _multiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrame multiFrame = e.FrameReference.AcquireFrame();
            if (this.EnableColorRecorder)
            {
                using (ColorFrame colorFrame = multiFrame.ColorFrameReference.AcquireFrame())
                {
                    if (colorFrame == null)
                    {
                        _recordQueue.Enqueue(null);
                    }
                    else
                    {
                        Bitmap bitmap = new Bitmap(_colorDescription.Width, _colorDescription.Height, PixelFormat.Format32bppRgb);
                        BitmapData bmData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                        IntPtr pNative = bmData.Scan0;
                        colorFrame.CopyConvertedFrameDataToIntPtr(pNative, _frameSize, ColorImageFormat.Bgra);
                        bitmap.UnlockBits(bmData);
                        _recordQueue.Enqueue(bitmap);
                    }
                }
            }
        }


        private string GetFileName(string fileName)
        {
            if (fileName == null)
            {
                string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);
                return string.Format("kinect{0}{1}", time, FILE_EXTENSION);
            }
            else
            {
                return fileName;
            }
        }

        private string GetDirectory(string path)
        {
            if (path == null)
            {
                return Directory.GetCurrentDirectory();
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return path;
            }
        }

        #endregion

        public string FullPath { get { return System.IO.Path.Combine(_path, _fileName); } }
    }
}
