﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kungly.KinectVideoRecorder
{
    public class KinectDataFrame
    {
        private IntPtr _dataPtr = IntPtr.Zero;
        private int _dataSize = 0;

        public KinectDataFrame(IntPtr dataPtr, int dataSize)
        {
            this.DataPtrt = dataPtr;
            this.DataSize = dataSize;
        }


        public IntPtr DataPtrt
        {
            get { return _dataPtr; }
            set { _dataPtr = value; }
        }

        public int DataSize
        {
            get { return _dataSize; }
            set { _dataSize = value; }
        }
        
        
    }
}
