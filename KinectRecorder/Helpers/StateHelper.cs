﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Kungly.KinectVideoRecorder
{
    public class StateHelper
    {
        public static string GetState(DependencyObject obj) 
        { return (string)obj.GetValue(StateProperty); }

        public static void SetState(DependencyObject obj, string value) 
        { obj.SetValue(StateProperty, value); }

        public static readonly DependencyProperty StateProperty = DependencyProperty.RegisterAttached(
            "State",
            typeof(String),
            typeof(StateHelper),
            new PropertyMetadata(null, StateChanged));


        internal static void StateChanged(DependencyObject target, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue != null)
                if (target is FrameworkElement)
                {
                    string newState = (string)args.NewValue;
                    ExtendedVisualStateManager.GoToElementState(target as FrameworkElement, (string)args.NewValue, true);
                }
        }
    }
}
