// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved


using Windows.ApplicationModel.Resources;

namespace Kungly.KinectVideoRecorder
{
    public static class ErrorMessagesHelper
    {
        private static readonly ResourceLoader _resourceLoader = new ResourceLoader();

        public static string FileNameRequired
        {
            get { return _resourceLoader.GetString("FileNameRequired"); }
        }
        public static string InvalidFileName
        {
            get { return _resourceLoader.GetString("InvalidFileName"); }
        }

    }
}
