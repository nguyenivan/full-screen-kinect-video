﻿using Kungly.KinectVideoRecorder.Views;
using Microsoft.Practices.Prism.StoreApps;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;


// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace Kungly.KinectVideoRecorder
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : MvvmAppBase
    {
        /// <summary>   
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
        }

        protected override Task OnLaunchApplication(
     LaunchActivatedEventArgs args)
        {
            this.NavigationService.Navigate("Main", null);
            return (Task.FromResult<object>(null));
        }

        protected override void OnInitialize(IActivatedEventArgs args)
        {
            ViewModelLocator.Register(typeof(MainPage).ToString(),
                                      () => new MainPageModel(NavigationService));
            ViewModelLocator.Register(typeof(SaveVideoPage).ToString(),
                                      () => new SaveVideoPageModel(NavigationService));
        }
    }
}
