﻿

//Additional information: The page name SaveVideoPage does not have an associated type in namespace Kungly.KinectVideoRecorder.Views

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

using Windows.UI.Xaml.Controls;
namespace Kungly.KinectVideoRecorder.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SaveVideoPage : Page
    {
        public SaveVideoPage()
        {
            this.InitializeComponent();
        }

    }
}
