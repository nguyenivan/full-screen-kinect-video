﻿using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using WindowsPreview.Kinect;

namespace Kungly.KinectVideoRecorder
{
    public class MainPageModel : ViewModel
    {

        INavigationService _navigationService;

        private KinectSensor _kinectSensor;
        private WriteableBitmap _colorBitmap;
        private MultiSourceFrameReader _multiFrameReader;
        private FrameDescription _colorFrameDescription;

        private bool _colorFrameProcessed;
        private bool m_isRecording;
        private KinectRecorder _recorder;

        private string _state;

                            private int _frameCount = 0;
                            private TimeSpan _avgTimeSpan = TimeSpan.FromTicks(0);
                            private TimeSpan _lastRelativeTime = TimeSpan.FromTicks(0);

        public ICommand RecordCommand { get; set; }

        public MainPageModel(INavigationService navigationService)
        {
            //VisualStateManager.GoToState(this, "KinectNotReady", true);

            // get the kinectSensor object
            this._kinectSensor = KinectSensor.GetDefault();

            _multiFrameReader = _kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color);

            _multiFrameReader.MultiSourceFrameArrived += _multiFrameReader_MultiSourceFrameArrived;

            // create the colorFrameDescription from the ColorFrameSource using Bgra format
            _colorFrameDescription = this._kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);

            // create the bitmap to display
            _colorBitmap = new WriteableBitmap(_colorFrameDescription.Width, _colorFrameDescription.Height);

            // set IsAvailableChanged event notifier
            _kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            _kinectSensor.Open();

            _navigationService = navigationService;

            RecordCommand = new AsyncCommand( () => Record() );

            //this.State = "KinectNotReady";

        }


        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                OnPropertyChanged("State");
            }
        }

        void _multiFrameReader_MultiSourceFrameArrived(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs args)
        {
            using (var multiFrame = args.FrameReference.AcquireFrame())
            {
                if (multiFrame != null)
                    using (var colorFrame = multiFrame.ColorFrameReference.AcquireFrame())
                    {
                        if (colorFrame != null)
                        {

                            if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                            {
                                colorFrame.CopyRawFrameDataToBuffer(_colorBitmap.PixelBuffer);
                            }
                            else
                            {
                                colorFrame.CopyConvertedFrameDataToBuffer(_colorBitmap.PixelBuffer, ColorImageFormat.Bgra);
                            }

                            _colorFrameProcessed = true;

                            if (_lastRelativeTime.Ticks > 0)
                            {
                                _avgTimeSpan = TimeSpan.FromTicks((_avgTimeSpan.Ticks * _frameCount + colorFrame.RelativeTime.Ticks - _lastRelativeTime.Ticks) / (_frameCount + 1));
                                //Debug.WriteLine("{0} <<< {1}", _frameCount, _avgTimeSpan);
                                _frameCount++;
                            }
                            _lastRelativeTime = colorFrame.RelativeTime;

                        }
                    }
            }

            // we got a frame, render
            if (_colorFrameProcessed)
            {
                _colorBitmap.Invalidate();
            }

        }

        private async Task Record()
        {
            if (m_isRecording)
            {
                m_isRecording = false;
                //m_writer.Close();
                using (_recorder)
                {
                    //this.StatusText = "Finishing ...";
                    await _recorder.StopAsync();
                    this.State = "Ready";
                    _navigationService.Navigate("SaveVideo", new VideoInfo(_recorder.Folder, _recorder.FileName));
                }
            }
            else
            {
                m_isRecording = true;
                _recorder = new KinectRecorder(null, _kinectSensor);
                _recorder.EnableColorRecorder = true;
                ///TODO: if _avgTimeSpan == 0 we have to wait
                await _recorder.Start((int)_avgTimeSpan.TotalMilliseconds);
                this.State = "Recording";
            }
        }

        private void Sensor_IsAvailableChanged(KinectSensor sender, IsAvailableChangedEventArgs args)
        {
            if (args.IsAvailable)
            {
                this.State = "Ready";
            }
            else
            {
                this.State = "KinectNotReady";
            }
        }

        public ImageSource ImageSource
        {
            get
            {
                return _colorBitmap;
            }
        }

        //protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChangedEventHandler eventHandler = this.PropertyChanged;
        //    if (eventHandler != null)
        //    {
        //        eventHandler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

    }
}
