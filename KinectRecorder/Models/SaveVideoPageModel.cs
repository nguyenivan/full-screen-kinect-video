﻿using Microsoft.Practices.Prism.StoreApps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;

namespace Kungly.KinectVideoRecorder
{
    public class SaveVideoPageModel : ViewModel
    {
        private VideoInfo _videoInfo = null;
        private VideoInfo _tempVideo = null;
        private ReadOnlyCollection<string> _allErrors;
        private Microsoft.Practices.Prism.StoreApps.Interfaces.INavigationService _navigationService;

        public SaveVideoPageModel(Microsoft.Practices.Prism.StoreApps.Interfaces.INavigationService NavigationService)
        {
            // TODO: Complete member initialization
            _navigationService = NavigationService;
            _allErrors = BindableValidator.EmptyErrorsCollection;
            ValidateAndSaveCommand = new AsyncCommand(() => Validate());
            CancelCommand = new AsyncCommand(() => Cancel());

        }

        private async Task Cancel()
        {
            bool err = false;
            try
            {
                StorageFile tempFile = await _tempVideo.Folder.GetFileAsync(_tempVideo.FileName);
                await tempFile.DeleteAsync();
            }
            catch (Exception e)
            {
                err = true;
                AllErrors = new ReadOnlyCollection<string>(new List<string>() { e.Message });
            }
            if (err)
            {
                Task.Delay(1000).Wait();
            }// Report error before going back

            _navigationService.Navigate("Main", null);
        }

        private async Task Validate()
        {
            if (_videoInfo.ValidateProperties())
            {
                if (!_tempVideo.IsEqual(_videoInfo))
                {
                    try
                    {
                        StorageFile tempFile = await _tempVideo.Folder.GetFileAsync(_tempVideo.FileName);
                        await tempFile.MoveAsync(_videoInfo.Folder, _videoInfo.FileName, NameCollisionOption.FailIfExists);
                        _navigationService.Navigate("Main", null);
                        
                    }
                    catch (Exception e)
                    {
                        AllErrors = new ReadOnlyCollection<string>(new List<string>() { e.Message });
                    }
                }
                else
                {
                    _navigationService.Navigate("Main", null);
                }
            }

        }

        private void OnErrorsChanged(object sender, System.ComponentModel.DataErrorsChangedEventArgs e)
        {
            AllErrors = new ReadOnlyCollection<string>(_videoInfo.GetAllErrors().Values.SelectMany(c => c).ToList());
        }

        public ICommand ValidateAndSaveCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public VideoInfo VideoInfo
        {
            get { return _videoInfo; }
            set
            {
                if (_videoInfo != null)
                {
                    _videoInfo.ErrorsChanged -= OnErrorsChanged;
                }
                SetProperty(ref _videoInfo, value);
                if (_videoInfo != null)
                {
                    _videoInfo.ErrorsChanged += OnErrorsChanged;
                }
            }
        }

        public ReadOnlyCollection<string> AllErrors
        {
            get { return _allErrors; }
            private set { SetProperty(ref _allErrors, value); }
        }

        public override void OnNavigatedTo(object navigationParameter, Windows.UI.Xaml.Navigation.NavigationMode navigationMode, Dictionary<string, object> viewModelState)
        {
            _tempVideo = navigationParameter as VideoInfo;
            this.VideoInfo = new VideoInfo(KnownFolders.VideosLibrary, _tempVideo.FileName);
        }
    }
}
