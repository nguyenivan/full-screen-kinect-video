﻿using Microsoft.Practices.Prism.StoreApps;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Kungly.KinectVideoRecorder
{
    public class VideoInfo : ValidatableBindableBase
    {
        public VideoInfo(StorageFolder folder, string fileName)
        {
            _folder = folder;
            _fileName = fileName;
        }

        private string _fileName;
        private StorageFolder _folder;
        //private const string FileNamePattern = @"^[\w,\s-]+\.[A-Za-z]{3}$";
        private const string FileNamePattern = @"^[\w,\.,\s-]+$";
        [Required(ErrorMessageResourceType = typeof(ErrorMessagesHelper), ErrorMessageResourceName = "FileNameRequired")]
        [RegularExpression(FileNamePattern, ErrorMessageResourceType=typeof(ErrorMessagesHelper),ErrorMessageResourceName="InvalidFileName")]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        public StorageFolder Folder
        {
            get { return _folder; }
            set { _folder = value; }
        }

        public VideoInfo DeepCopy()
        {
            return new VideoInfo(_folder, _fileName);
        }

        public bool IsEqual (VideoInfo v) {
            try
            {
                if (Folder.IsEqual(v.Folder) && string.Equals(FileName.Trim(), v.FileName.Trim()))
                {
                    return true;
                }
            }
            catch (NullReferenceException)
            {
                return false;
            }
            return false;
        }

    }
}
